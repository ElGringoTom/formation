<?php

namespace spec\App\Security;

use App\Entity\Cake;
use App\Entity\User;
use App\Security\EditCakeVoter;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class EditCakeVoterSpec extends ObjectBehavior
{
    function let(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->beConstructedWith($authorizationChecker);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(EditCakeVoter::class);
    }

    function it_is_voter()
    {
        $this->shouldBeAnInstanceOf(VoterInterface::class);
    }

    function it_abstains_if_subject_is_not_cake(TokenInterface $token)
    {
        $this->vote($token, null, [])->shouldReturn(VoterInterface::ACCESS_ABSTAIN);
    }

    function it_abstains_if_attribute_not_contains_EDIT_CAKE(TokenInterface $token, Cake $cake)
    {
        $this->vote($token, $cake, [])->shouldReturn(VoterInterface::ACCESS_ABSTAIN);
    }

    function it_denies_if_user_not_connected(TokenInterface $token, Cake $cake)
    {
        $this->vote($token, $cake, ['EDIT_CAKE'])->shouldReturn(VoterInterface::ACCESS_DENIED);
    }

    function it_grants_if_user_connected_with_ROLE_ADMIN(TokenInterface $token,
                                                         AuthorizationCheckerInterface $authorizationChecker,
                                                         Cake $cake,
                                                         User $user)
    {

        $token->getUser()->willReturn($user);
        $authorizationChecker->isGranted('ROLE_ADMIN')->willReturn(true);
        $this->vote($token, $cake, ['EDIT_CAKE'])->shouldReturn(VoterInterface::ACCESS_GRANTED);
    }

    function it_denies_if_user_connected_without_ROLE_ADMIN_and_no_author(TokenInterface $token,
                                                                          AuthorizationCheckerInterface $authorizationChecker,
                                                                          Cake $cake,
                                                                          User $user,
                                                                          User $author)
    {

        $token->getUser()->willReturn($user);
        $authorizationChecker->isGranted('ROLE_ADMIN')->willReturn(false);
        $cake->getAuthor()->willReturn($author);
        $this->vote($token, $cake, ['EDIT_CAKE'])->shouldReturn(VoterInterface::ACCESS_DENIED);
    }

    function it_grants_if_user_connected_without_ROLE_ADMIN_and_is_author(TokenInterface $token,
                                                                          AuthorizationCheckerInterface $authorizationChecker,
                                                                          Cake $cake,
                                                                          User $user)
    {

        $token->getUser()->willReturn($user);
        $authorizationChecker->isGranted('ROLE_ADMIN')->willReturn(false);
        $cake->getAuthor()->willReturn($user);
        $this->vote($token, $cake, ['EDIT_CAKE'])->shouldReturn(VoterInterface::ACCESS_GRANTED);
    }
}
