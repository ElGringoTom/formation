<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateCakeCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:cake:create')
            ->setDescription('Create a new cake');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Cake creation ... ',
            '===========================',
            '',
        ]);

        $question = new Question('Saisissez le nom: ');
        $name = $this->getHelper('question')->ask($input, $output, $question);

        $output->writeln([
            'name = '.$name,
        ]);


        $output->writeln([
            'Creation done !',
        ]);

        return Command::SUCCESS;
    }

}