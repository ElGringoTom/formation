<?php

namespace App\Form\Type;

use App\Entity\Cake;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Math;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CakeType extends AbstractType
{

    /**
     * @param array<string,string> $options
     * @param FormBuilderInterface<FormType> $builder
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name of the cake',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 255])
                ],
                'attr' => [
                    'class' => 'form-control'
                ],
                'help' => 'Name'
            ])
            ->add('description', TextType::class, [
                'label' => 'Description of thee cake',
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 255])
                ]
            ])
            ->add('price', NumberType::class, [
                'label' => 'Price of the cake',
                'scale' => 2,
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\GreaterThan(0)
                ]
            ])
            ->add('image', UrlType::class, [
                'label' => 'Image of the cake',
                'attr' => [
                    'class' => 'form-control',
                    'value' => 'https://picsum.photos/200/300?random='.random_int(0, 1000)
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Url(),
                    new Assert\Length(['max' => 255])
                ]
            ])
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('c')
                    ->orderBy('c.name', 'DESC');
                },
                'multiple' => true,
                'expanded' => false
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Cake::class);
    }
}