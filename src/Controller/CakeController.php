<?php

namespace App\Controller;

use App\Entity\Cake;
use App\Form\Type\CakeType;
use App\Security\EditCakeVoter;
use App\Task\EditCake\EditCake;
use App\Task\EditCake\Input;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CakeController extends AbstractController
{
    private $editCake;

    public function __construct(EditCake $editCake)
    {
        $this->editCake = $editCake;
    }

    public function list(SessionInterface $session): Response
    {
        dump($session->get('KEY_TEST'));
        $cakeRepository = $this->getDoctrine()->getManager()->getRepository(Cake::class);
        $cakes = $cakeRepository->findAll();
        return $this->render(
            'cakes/list.html.twig',
            [
                'cakeList' => $cakes
            ]
        );
    }

    public function show(?Cake $cake): Response
    {
        if (!$cake) {
            throw $this->createNotFoundException('Cake not found');
        }

        return $this->render(
            'cakes/show.html.twig',
            [
                'cake' => $cake,
            ]
        );

    }

    public function edit(SessionInterface $session, Request $request, ?Cake $cake): Response
    {
        $session->set('KEY_TEST', 'test');
        if (!$this->isGranted(EditCakeVoter::ATTRIBUTE, $cake)) {
            $this->addFlash('danger', 'Access refused');
            return $this->redirectToRoute('app_cake_list');
        }

        if (!$cake) {
            throw $this->createNotFoundException('Cake not found');
        }

        return $this->createOrEdit($request, $cake);
    }

    public function create(Request $request): Response
    {
        return $this->createOrEdit($request);
    }

    private function createOrEdit(Request $request, ?Cake $cake = null)
    {
        $form = $this->createForm(CakeType::class, $cake);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $isEditing = $cake !== null;

                $em = $this->getDoctrine()->getManager();
                $cake = $form->getData();
                if ($cake instanceof Cake && !$isEditing)
                {
                    $cake->setAuthor($this->getUser());
                }

                $input = new Input($cake);
                $outputCake = (($this->editCake)($input))->getCake();


                $this->addFlash('success', $isEditing ? 'Cake '.$outputCake->getName().' sucessfully updated' : 'Cake '.$outputCake->getName().' sucessfully added');
                return $this->redirectToRoute('app_cake_list');
            } else {
                $this->addFlash('error', 'An error occured');
            }
        }

        return $this->render(
            'cakes/create.html.twig',
            [
                'cakeCreationForm' => $form->createView()
            ]
        );
    }
}