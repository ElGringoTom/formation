<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /** @var int */
    private $id;
    /** @var string */
    private $email;
    /** @var string */
    private $password;
    /** @var string[] */
    private $roles;
    /** @var string */
    private $salt;

    public function __construct($email)
    {
        $this->email = $email;
        $this->roles = ['ROLE_USER'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }


    public function addRole(string $role)
    {
        $this->roles[] = $role;
    }

    public function hasRole(string $role): bool
    {
        return (in_array($role, $this->roles));
    }

    public function removeRole(string $role)
    {
        if (array_key_exists($role, $this->roles))
        {
            unset($this->roles[$role]);
        }
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

}