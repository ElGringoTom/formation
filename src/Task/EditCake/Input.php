<?php

namespace App\Task\EditCake;

use App\Entity\Cake;

class Input
{
    private $cake;

    function __construct(Cake $cake)
    {
        $this->cake = $cake;
    }

    /**
     * @return Cake
     */
    public function getCake(): Cake
    {
        return $this->cake;
    }
}