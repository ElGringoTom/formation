<?php

namespace App\Task\EditCake;

use _HumbugBoxfb21822734fc\Nette\Neon\Exception;
use App\Exception\TaskException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class EditCake
{
    private $entityManager;
    private $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function __invoke(Input $input)
    {
        try {
            throw new Exception('hahahaha');
            $this->entityManager->persist($input->getCake());
            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->logger->error('[edit_cake] - An error occured while saving cake: '.$e->getMessage());
            throw new TaskException('[edit_cake] - An error occured while saving cake');
        }

        $this->logger->info('[edit_cake] - Cake succesfully saved: '.$input->getCake()->getName());

        return new Output($input->getCake());
    }
}