<?php

namespace App\Security;

use App\Entity\Cake;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class EditCakeVoter implements VoterInterface
{

    public const ATTRIBUTE = 'EDIT_CAKE';

    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        if (!$subject instanceof Cake) {
            return self::ACCESS_ABSTAIN;
        }

        if (!in_array(self::ATTRIBUTE, $attributes)) {
            return self::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN'))
        {
            return self::ACCESS_GRANTED;
        }

        if ($subject->getAuthor() !== $user) {
            return self::ACCESS_DENIED;
        }

        return self::ACCESS_GRANTED;
    }
}